<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/articlepdf?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_titre_parametrages' => 'Parámetros de los pdfs creados',
	'configurer_titre' => 'Configurar el plugin article_pdf',
	'copyright' => 'Copyright © ',

	// D
	'date_de_mise_en_ligne' => 'Fecha de publicación en línea: ',
	'date_de_parution' => 'Fecha de redacción: ',
	'description' => 'Descripción: ',

	// E
	'enregistrer_pdf' => 'Grabar en formato PDF',
	'extrait_de' => 'Extraído de ',

	// L
	'label_pdf_to_document' => 'Solamente en SPIP3',
	'label_pdf_to_document_oui' => 'Guardar y vincular pdf con el documento',

	// P
	'post_scriptum' => 'Posdata: ',

	// T
	'tous_droits_reserves' => ' - Todos derechos reservados'
);
