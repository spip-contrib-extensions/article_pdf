<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/articlepdf?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_titre_parametrages' => 'De gemaakte PDF’s van parameters voorzien',
	'configurer_titre' => 'De plugin article_pdf configureren',
	'copyright' => 'Copyright © ',

	// D
	'date_de_mise_en_ligne' => 'Online geplaatst op: ',
	'date_de_parution' => 'Verschijningsdatum: ',
	'description' => 'Omschrijving: ',

	// E
	'enregistrer_pdf' => 'In PDF-formaat opslaan',
	'extrait_de' => 'Uittreksel van ',

	// L
	'label_pdf_to_document' => 'Alleen in SPIP3',
	'label_pdf_to_document_oui' => 'De PDF registreren en koppelen als document',

	// P
	'post_scriptum' => 'PS: ',

	// T
	'tous_droits_reserves' => ' - Alle rechten voorbehouden'
);
