<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/articlepdf?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_titre_parametrages' => 'Configure the created pdfs',
	'configurer_titre' => 'Configure the article_pdf plugin',
	'copyright' => 'Copyright © ',

	// D
	'date_de_mise_en_ligne' => 'Publication date: ',
	'date_de_parution' => 'Creation date: ',
	'description' => 'Description: ',

	// E
	'enregistrer_pdf' => 'Save this article in PDF',
	'extrait_de' => 'Extract of ',

	// L
	'label_pdf_to_document' => 'Only in  SPIP3',
	'label_pdf_to_document_oui' => 'Save and link pdf as document',

	// P
	'post_scriptum' => 'PS: ',

	// T
	'tous_droits_reserves' => ' - All rights reserved'
);
