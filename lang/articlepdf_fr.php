<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/article_pdf.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_titre_parametrages' => 'Paramétrer les pdfs créés',
	'configurer_titre' => 'Configurer le plugin article_pdf',
	'copyright' => 'Copyright © ',

	// D
	'date_de_mise_en_ligne' => 'Date de mise en ligne : ',
	'date_de_parution' => 'Date de parution : ',
	'description' => 'Description : ',

	// E
	'enregistrer_pdf' => 'Enregistrer au format PDF',
	'extrait_de' => 'Extrait du ',

	// L
	'label_pdf_to_document' => 'Seulement si je suis en SPIP3',
	'label_pdf_to_document_oui' => 'Enregistrer et lier le pdf en tant que document',

	// P
	'post_scriptum' => 'Post-scriptum : ',

	// T
	'tous_droits_reserves' => ' - Tous droits réservés'
);
